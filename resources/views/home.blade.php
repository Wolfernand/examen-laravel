@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (Auth::guest())
                    <p>Please login with your Credentials or Register a new user.</p>
                    @else
                    <p>Welcome to Laravel Test, Please Select an option from the Menu.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
