<!DOCTYPE html>
<html>
<head>
	<title>Laravel Test</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container">

		<nav class="navbar navbar-inverse">
			<div class="navbar-header">
				<a class="navbar-brand">LaravelTest</a>
			</div>
			<ul class="nav navbar-nav">
				@if (Auth::check())
				@else
				<li><a href="{{ url('/home') }}">Home</a></li>
				<li><a href="{{ URL::to('users') }}">View All users</a></li>
				<li><a href="{{ URL::to('users/create') }}">Create a User</a></li>

				
				<li><a href="{{ url('/login') }}">Login</a></li>
				@endif
			</ul>
		</nav>
	</div>