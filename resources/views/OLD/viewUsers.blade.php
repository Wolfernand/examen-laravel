@include('partials.header')
<div class="container">
	<h1>Users</h1>
	<p>
		Here you can see al the Data that is stored in the table Users
	</p>

	<table class="table">
		<caption>Registered Users</caption>
		<thead>
			<tr>
				<th>ID</th>
				<th>Email</th>
				<th>Rol</th>
				<th>Created</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td>{{$user->id}}</td>
				<td>{{$user->email}}</td>
				<td>{{$user->type}}</td>
				<td>{{$user->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@include('partials.footer')
