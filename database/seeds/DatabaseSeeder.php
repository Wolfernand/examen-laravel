<?php

use Illuminate\Database\Seeder;
use App\User as User;
use App\Customer as Customer;

class DatabaseSeeder extends Seeder {

    public function run()
    {
    	Eloquent::unguard();
		//Disable foreign key check for this connection before running seeders
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call('UserTableSeeder');
        $this->call('CustomerTableSeeder');

        $this->command->info('User table seeded!');
        //Enable Foreign Check
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        //DB::table('users')->delete();

        //User::create(['email' => 'foo@bar.com']);
        User::truncate();
          
        User::create( [
            'email' => 'admin@laravel.com' ,
            'password' => Hash::make( 'admin' ) ,
            'type' => 'admin' ,
        ] );
          
        User::create( [
            'email' => 'customer1@laravel.com' ,
            'password' => Hash::make( 'customer' ) ,
            'type' => 'customer' ,
        ] );
          
        /*User::create( [
            'email' => 'customer2@laravel.com' ,
            'password' => Hash::make( 'customer' ) ,
            'type' => 'customer' ,
        ] );
          
        User::create( [
            'email' => 'customer3@laravel.com' ,
            'password' => Hash::make( 'customer' ) ,
            'type' => 'customer' ,
        ] );*/
    }

}

class CustomerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('customers')->delete();

        Customer::create( [
            'name' => 'Administrator' ,
            'address' => 'General Pablo Gonzalez 871' ,
            'RFC' => 'VERW911172HCC' ,
            'user_id' => '1' ,
        ] );
        Customer::create( [
            'name' => 'Customer 1' ,
            'address' => 'El Chanal 785' ,
            'RFC' => 'KIDY764932KKA' ,
            'user_id' => '1' ,
        ] );
    }

}