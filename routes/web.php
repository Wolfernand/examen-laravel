<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::resource ('/users', 'UserController');
*/
Route::get('/', function () {
    return view('/home');
});

Route::get('/profile', function () {
	if(Auth::check()){
		$user_id = Auth::user()->id;
		$data['user'] = App\User::get();
    	return view('/profile',$data);
	}else{
		return redirect('/home');
	}
});

Route::get('/users', function () {
	if(Auth::check()){
		if(Auth::user()->type == "admin"){
			$data['users'] = "";
    		return view('users.viewUsers',$data);
    	}else{
    		Auth::logout();
			return redirect('/home');
    	}
	}else{
		return redirect('/home');
	}
});

/*Route::get('/users/', function () {
	if(Auth::check()){
		if(Auth::user()->type == "admin"){
			$data['users'] = App\User::all();
    		return view('/viewUsers',$data);
    	}else{
    		Auth::logout();
			return redirect('/home');
    	}
	}else{
		return redirect('/home');
	}
});

Route::get('/users/', function () {
	if(Auth::check()){
		if(Auth::user()->type == "admin"){
			$data['users'] = App\User::all();
    		return view('/viewUsers',$data);
    	}else{
    		Auth::logout();
			return redirect('/home');
    	}
	}else{
		return redirect('/home');
	}
});*/

/*Route::get('/users', function () {
    return view('viewUsers');
});

Route::get('/customers', function () {
    return view('viewCustomers');
});
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
